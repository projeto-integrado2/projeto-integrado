# Siac - Projeto integrado

Bem-vindo ao SIAC (Sistema de Acesso a Chaves) do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Norte (IFRN). Este sistema foi desenvolvido para facilitar o controle e gerenciamento das chaves em nossa escola.

## Instalação

Para executar este sistema, você precisa ter o Node.js instalado em sua máquina. Siga os passos abaixo para configurar o projeto:

1. Clone o repositório:

2. Navegue até o diretório do projeto:

3. Instale as dependências do servidor Node.js:

4. Instale as dependências do cliente React:

5. Inicie o servidor Node.js:

6. Inicie o cliente React:

O sistema estará disponível em `#link da API`.

## Uso

O SIAC oferece as seguintes funcionalidades:

- Registro e empréstimo de chaves por parte dos alunos e funcionários.
- Consulta de chaves disponíveis e emprestadas.
- Histórico de empréstimos.
- Gerenciamento de chaves por parte da administração.

Certifique-se de documentar todas as funcionalidades e recursos do seu sistema no README.

## Contribuição

Sinta-se à vontade para contribuir para o desenvolvimento deste projeto. Para fazer uma contribuição, siga estes passos:

1. Faça um fork do repositório.
2. Crie uma nova branch com sua feature: `git checkout -b siac-projeto-integrado`.
3. Faça commit das suas alterações: `git commit -m 'Adicionei uma nova funcionalidade'`.
4. Envie suas alterações para a branch principal: `git push origin siac-projeto-integrado`.
5. Abra um Pull Request descrevendo suas alterações.

## Licença

Este projeto é licenciado sob a Licença GPL v3 - Veja o arquivo [LICENSE](LICENSE) para obter detalhes.

## Contato

Para perguntas ou informações adicionais, entre em contato conosco em [email@escolar.ifrn.edu.br](mailto:@escolar.ifrn.edu.br).
